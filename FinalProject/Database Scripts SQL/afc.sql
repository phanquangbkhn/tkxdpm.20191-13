-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th12 28, 2019 lúc 09:26 AM
-- Phiên bản máy phục vụ: 10.4.6-MariaDB
-- Phiên bản PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `afc`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `card`
--
create database afc;
use afc;

CREATE TABLE `card` (
  `id` varchar(14) NOT NULL,
  `code` varchar(16) NOT NULL,
  `status` varchar(45) NOT NULL,
  `balance` double NOT NULL,
  `createAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `card`
--

INSERT INTO `card` (`id`, `code`, `status`, `balance`, `createAt`) VALUES
('PC201610262000', '9ac2197d9258257b', 'new', 30, '2019-10-26 12:00:01'),
('PC201610262001', 'e0af71fe71d9253d', 'new', 20, '2019-10-26 12:00:01'),
('PC201610262002', '9e80a533d37dc63f', 'new', 25, '2019-10-26 12:00:01');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `station`
--

CREATE TABLE `station` (
  `id` int(11) NOT NULL,
  `name` varchar(45) CHARACTER SET latin1 NOT NULL,
  `location` float NOT NULL,
  `zone` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

--
-- Đang đổ dữ liệu cho bảng `station`
--

INSERT INTO `station` (`id`, `name`, `location`, `zone`) VALUES
(1, 'Saint-Lazare', 0, 1),
(2, 'Madeleinne', 1.6, 2),
(3, 'Pyramides', 3.2, 3),
(4, 'Chatelet', 4.4, 1),
(5, 'Gare de Lyon', 5.2, 2),
(6, 'Bercy', 6.8, 3),
(7, 'Cour Saint-Emilion', 8.1, 2),
(8, 'Bibliotheque Francois Mitterrand', 9.4, 1),
(9, 'Olympiades', 12.5, 3);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ticket_24h`
--

CREATE TABLE `ticket_24h` (
  `id` varchar(14) NOT NULL,
  `code` varchar(45) NOT NULL,
  `status` varchar(45) NOT NULL,
  `price` double NOT NULL,
  `first_time_use` datetime DEFAULT NULL,
  `createAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `ticket_24h`
--

INSERT INTO `ticket_24h` (`id`, `code`, `status`, `price`, `first_time_use`, `createAt`) VALUES
('TF201910260000', 'e8dc4081b13434b4', 'new', 8.5, NULL, '2019-10-25 10:10:10'),
('TF201910260001', '6d3f33e7ac1f9cbb', 'in', 8.5, '2019-12-28 14:51:13', '2019-10-25 10:10:10'),
('TF201910260002', 'ea9d3d1e279e8fd6', 'new', 8.5, NULL, '2019-10-25 10:10:10'),
('TF201912080003', 'bc179d76a1e1d1a5', 'new', 8.5, NULL, '2019-12-09 10:59:29'),
('TF201912080004', '2a2c3b88bcf97a5c', 'new', 8.5, NULL, '2019-12-06 11:59:29');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ticket_one_way`
--

CREATE TABLE `ticket_one_way` (
  `id` varchar(14) NOT NULL,
  `code` varchar(45) NOT NULL,
  `status` varchar(45) NOT NULL,
  `price` double NOT NULL,
  `stationIn` int(11) NOT NULL,
  `stationOut` int(11) NOT NULL,
  `createAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `ticket_one_way`
--

INSERT INTO `ticket_one_way` (`id`, `code`, `status`, `price`, `stationIn`, `stationOut`, `createAt`) VALUES
('OW201910261000', 'edcd20bc35ae4b0b', 'in', 1.9, 1, 3, '2019-11-09 12:10:00'),
('OW201910261001', '4594bb26f2f93c5c', 'destroy', 2.3, 1, 5, '2019-11-09 12:10:00'),
('OW201910261002', 'e59995de368c2adf', 'new', 2.7, 1, 7, '2019-11-09 12:10:00'),
('OW201910261003', '236304c2a3e505cb', 'new', 3.1, 1, 8, '2019-11-09 12:10:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `trips`
--

CREATE TABLE `trips` (
  `id` int(11) NOT NULL,
  `tc_id` varchar(14) NOT NULL,
  `stationInReal` int(11) NOT NULL,
  `stationOutReal` int(11) DEFAULT NULL,
  `stationInTime` datetime NOT NULL,
  `stationOutTime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `trips`
--

INSERT INTO `trips` (`id`, `tc_id`, `stationInReal`, `stationOutReal`, `stationInTime`, `stationOutTime`) VALUES
(1, 'TF201910260000', 1, 1, '2019-12-16 13:12:24', '2019-12-16 13:39:07'),
(2, 'TF201910260000', 1, NULL, '2019-12-16 13:38:55', NULL),
(3, 'OW201910261000', 1, 1, '2019-12-16 13:39:20', '2019-12-16 13:39:32'),
(4, 'OW201910261000', 1, NULL, '2019-12-16 13:40:25', NULL),
(5, 'TF201910260001', 1, NULL, '2019-12-28 14:51:13', NULL),
(6, 'OW201910261001', 1, 6, '2019-12-28 14:51:59', '2019-12-28 14:52:19');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `card`
--
ALTER TABLE `card`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `station`
--
ALTER TABLE `station`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `ticket_24h`
--
ALTER TABLE `ticket_24h`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `ticket_one_way`
--
ALTER TABLE `ticket_one_way`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stationIn` (`stationIn`),
  ADD KEY `stationOut` (`stationOut`);

--
-- Chỉ mục cho bảng `trips`
--
ALTER TABLE `trips`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stationOutReal` (`stationOutReal`),
  ADD KEY `station_in_real_trip` (`stationInReal`);

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `ticket_one_way`
--
ALTER TABLE `ticket_one_way`
  ADD CONSTRAINT `stationIn` FOREIGN KEY (`stationIn`) REFERENCES `station` (`id`),
  ADD CONSTRAINT `stationOut` FOREIGN KEY (`stationOut`) REFERENCES `station` (`id`);

--
-- Các ràng buộc cho bảng `trips`
--
ALTER TABLE `trips`
  ADD CONSTRAINT `station_in_real_trip` FOREIGN KEY (`stationInReal`) REFERENCES `station` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
