package com.tkxdpm.utils;

public class CheckLowerString {

	/**
	 * This function is used to check one String is lower case string
	 * @param st String input
	 * @return boolean If all Character of string is lower case, return true, else return false
	 */
	public static boolean checkLowerString(String st) {
		for (int i = 0; i < st.length() - 1; i++) {
			if (Character.isUpperCase(st.charAt(i))) {
				return false;
			}
		}
		return true;
	}
}
