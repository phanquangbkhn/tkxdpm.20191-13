package com.tkxdpm.utils;

public class CheckUpperString {
	
	/**
	 * This function is used to check one String is upper case string
	 * @param st String input
	 * @return boolean If all Character of string is upper case, return true, else return false
	 */
		public static boolean checkUpperString(String st) {
			for(int i = 0; i < st.length() - 1; i++) {
				if(Character.isLowerCase(st.charAt(i))) {
					return false;
				}
			}
			return true;
		}
}
