package com.tkxdpm.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;

public class ReadFile {
	/**
	 * This function is used to read data from input file
	 * @param path Path of file input
	 * @return one list array string, Each element is a line of the file
	 */
	public static ArrayList<String> readFile(String path){
		ArrayList<String> list = new ArrayList<String>();
		BufferedReader bfr = null;
		
		try {
			Reader fr = new FileReader(path);
			bfr = new BufferedReader(fr);
			String line = bfr.readLine();
			while(line != null) {
				list.add(line);
				line = bfr.readLine();
			}
			
			fr.close();
			bfr.close();
			
		} catch(IOException e) {
			System.out.println("Read file fail");
		}
		return list;
	}
}
