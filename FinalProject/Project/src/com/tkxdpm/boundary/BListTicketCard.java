package com.tkxdpm.boundary;


import java.util.Map;
import java.util.Scanner;
import com.tkxdpm.controller.AFCController;
import com.tkxdpm.controller.CardController;
import com.tkxdpm.controller.EntityController;
import com.tkxdpm.controller.Ticket24hController;
import com.tkxdpm.controller.TicketOnewayController;
import com.tkxdpm.entity.Card;
import com.tkxdpm.entity.Entity;
import com.tkxdpm.entity.Ticket24h;
import com.tkxdpm.entity.TicketOneway;


public class BListTicketCard {
	// exitOrEnterInput = 1 for check-in and exitOrEnterInput = 2 for check-out
	private Character exitOrEnterInput;
	private int indexStation;
	private Scanner scanner;
	public BListTicketCard() {

	}
	
	public BListTicketCard(Character exitOrEnterInput, int indexStaion) {
		this.exitOrEnterInput = exitOrEnterInput;
		this.indexStation = indexStaion;
	}

	public Character getExitOrEnterInput() {
		return exitOrEnterInput;
	}

	public void setExitOrEnterInput(Character exitOrEnterInput) {
		this.exitOrEnterInput = exitOrEnterInput;
	}

	public int getIndexStation() {
		return indexStation;
	}

	public void setIndexStation(int indexStaion) {
		this.indexStation = indexStaion;
	}

	/**
	 * This function print all tickets and cards from file input
	 * @param mapBarcodeEntity This is a map contains object is ticket, card with key is barcode
	 */
	
	public void printListTicketCard(Map <String, Entity> mapBarcodeEntity) {
		System.out.println("2. Choosing a Ticket/Card\n\n");
		System.out.println("There are existing tickets/cards:\n");
		for (Map.Entry<String, Entity> entry : mapBarcodeEntity.entrySet()) {
			Entity entity = entry.getValue();
			String barcode = entry.getKey();
			if (entity instanceof TicketOneway) {
				System.out.print(barcode + ". ");
				System.out.println(entity.toString());
			} else if (entity instanceof Ticket24h) {
				System.out.print(barcode + ". ");
				System.out.println(entity.toString());
			} else {
				System.out.print(barcode + ". ");
				System.out.println(entity.toString());
			}
		}
	}
	
	/**
	 * This function is used to entered barcode from keyboard
	 * @return String barcode from keyboard
	 */
	public String getBarcode() {
		scanner = new Scanner(System.in);
		System.out.println();
		System.out.println("Please provide the ticket/card barcode you want to enter/exit: ");
		String barcode = scanner.nextLine();
		return barcode;
	}
	

	/**
	 * this function to call to boudary class
	 */
	public void handleBarcode(){
		AFCController afcController = new AFCController();
		String barcode = getBarcode();
		Entity entity = afcController.getEntityByBarcode(barcode);
		
		String message = null;
		if (entity instanceof TicketOneway) {
			TicketOneway ticketOneway = (TicketOneway) entity;
			EntityController entityController = new TicketOnewayController(ticketOneway, this.indexStation);
			afcController.setEntityController(entityController);
			if(this.exitOrEnterInput == '1') {
				message = afcController.getEntityController().checkIn();
			}else {
				message = afcController.getEntityController().checkOut();
			}
			BTicketOneway bticketOneway = new BTicketOneway();
			bticketOneway.printResultTicketOneway(ticketOneway, message);
		}
		else if(entity instanceof Ticket24h){
			Ticket24h ticket24h = (Ticket24h) entity;
			Ticket24hController ticket24hC = new Ticket24hController(ticket24h, this.indexStation);
			afcController.setEntityController(ticket24hC);
			if(this.exitOrEnterInput == '1') {
				message = afcController.getEntityController().checkIn();
			}else {
				message = afcController.getEntityController().checkOut();
			}
			BTicket24h bTicket24h = new BTicket24h();
			bTicket24h.printResultTicket24h(ticket24h, message);
		}
		else if (entity instanceof Card) {
			Card card = (Card) entity;
			EntityController cardC = new CardController(card, this.indexStation);
			afcController.setEntityController(cardC);
			if(this.exitOrEnterInput == '1') {
				message = afcController.getEntityController().checkIn();
			}else {
				message = afcController.getEntityController().checkOut();
			}
			BCard bCard = new BCard();
			bCard.printResultCard(card, message);
		}
		if (message.equals("OK")) {
			afcController.openAndCloseGate();
		}
	}
}
