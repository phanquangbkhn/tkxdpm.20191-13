package com.tkxdpm.boundary;

import com.tkxdpm.entity.Card;

public class BCard {
	/**
	 * 
	 * This method is print check result
	 * 
	 * @param card    This is information of card object
	 * @param message String This is an error message if any
	 */
	public void printResultCard(Card card, String message) {
		if (message == "OK") {
			System.out.println("Type: Card");
			System.out.println("ID: " + card.getID());
			System.out.println("Balance: " + card.getBalance() + "$");
		} else {
			System.out.println("Type: Card");
			System.out.println("ID: " + card.getID());
			System.out.println("Balance: " + card.getBalance() + "$");
			System.out.println(message);
		}
	}
}
