package com.tkxdpm.boundary;

import com.tkxdpm.entity.TicketOneway;

public class BTicketOneway {
	
	/**
	 * 
	 * This method is print check result
	 * 
	 * @param ticketOneway This is information of one way ticket object
	 * @param message String This is an error message if any
	 */
	public void printResultTicketOneway(TicketOneway ticketOneway, String message) {
		System.out.println("Type: Oneway-ticket");
		System.out.println("ID: " + ticketOneway.getID());
		System.out.println("Price: " + ticketOneway.getPrice() + "$");
		if (message != "OK") {
			System.out.println(message);
		}
	}
}
