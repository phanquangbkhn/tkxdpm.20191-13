package com.tkxdpm.controller;

import java.sql.Timestamp;
import java.util.Date;
import com.tkxdpm.dao.Ticket24hDAO;
import com.tkxdpm.dao.TripDAO;
import com.tkxdpm.entity.Ticket24h;
import com.tkxdpm.entity.Trip;

public class Ticket24hController extends EntityController {
	private Ticket24h ticket24h;

	public Ticket24hController() {

	}

	public Ticket24hController(Ticket24h ticket24h, int idStation) {
		this.ticket24h = ticket24h;
		this.idStation = idStation;
	}

	public void setTicket24h(Ticket24h ticket24h) {
		this.ticket24h = ticket24h;
	}

	public void setIDStation(int idStation) {
		this.idStation = idStation;
	}

	/**
	 * This function is used to check in Ticket24h's status
	 * 
	 * @param ticket24h Instance of Ticket24h class
	 * @return String Ticket24h has four status new, out, in and destroy. This
	 *         function returns one of four
	 */

	/**
	 * This function is check status 
	 * @return error message if status is not "new" or "out", else return null
	 */
	public String checkStatusIn() {
		switch (ticket24h.getStatus()) {
		case "new":
			return null;
		case "out":
			return null;
		case "in":
			return "Ticket's status is \"In\"" + "!!! Please choose other ticket!";
		default:
			return "Ticket's status is \"Destroy\"" + "!!! Please choose other ticket!";
		}
	}

	/**
	 * This function is check status is "new"
	 * @return true if status is "new", else return "false"
	 */
	public boolean checkStatusNew() {
		if (ticket24h.getStatus().equals("new")) {
			return true;
		}
		return false;
	}



	/**
	 * This function is set first time use of ticket is now
	 */
	public void setFirstTimeUse() {
		Date now = new Date();
		Timestamp timestamp = new Timestamp(now.getTime());
		ticket24h.setFirstTimeUse(timestamp);
		Ticket24hDAO t24hDAO = new Ticket24hDAO();
		t24hDAO.updateFirstTimeUse(ticket24h);
	}

	/**
	 * this is function check in 24h ticket
	 */
	public String checkIn() {
		String statusMessage = checkStatusIn();
		if (statusMessage != null) {
			return statusMessage;
		}
		if (checkStatusNew() == true) {
			setFirstTimeUse();
		}
		// change status
		ticket24h.setStatus("in");
		ticket24h.changeStatusIn();
		// save strip
		generateTrip();
		return "OK";
	}

	/**
	 * This function is used to generate a trip and save it to database
	 */
	public void generateTrip() {
		TripDAO tripDAO = new TripDAO();
		Trip trip = new Trip();
		// setID
		int idTrip = tripDAO.getMaxID() + 1;
		trip.setID(idTrip);
		// setTimenow
		Date now = new Date();
		Timestamp timestamp = new Timestamp(now.getTime());
		trip.setStationInTime(timestamp);
		tripDAO.insertTrip(trip, ticket24h.getID(), idStation);
	}

	/**
	 * 
	 * @return message error if fail or null if is ok
	 */
	@Override
	public String checkStatusOut() {
		switch (ticket24h.getStatus()) {
		case "new":
			return "The ticket's status is \"New\"!" + "\nYou must checkin your ticket before checkout it.";
		case "in":
			return null;
		case "out":
			return "The ticket's status is \"Out station\"!" + "\nYou must checkin your ticket before checkout it.";
		default:
			return "The ticket's status is \"Destroy\"!";
		}
	}

	/**
	 * 
	 * @param firstTimeUse This is first time use ticket 24h
	 * @return true if time bellow, false if time over
	 */
	private boolean checkExpired() {
		Timestamp firstTimeUse = ticket24h.getFirstTimeUse();
		Timestamp currTimestamp = new Timestamp(System.currentTimeMillis());
		long timeUse = (currTimestamp.getTime() - firstTimeUse.getTime()) / 1000 / 60 / 60;
		if (timeUse <= 24.0) {
			return true;
		}
		return false;
	}

	/**
	 * This function is checkout 24h ticket
	 */
	@Override
	public String checkOut() {
		String statusMessage = checkStatusOut();
		if (statusMessage != null) {
			return statusMessage;
		} else {
			if (checkExpired() == true) {
				ticket24h.setStatus("out");
				ticket24h.changeStatusOut();
			} else {
				ticket24h.setStatus("destroy");
				ticket24h.changeStatusOut();
			}
			// save trip
			TripDAO tripDAO = new TripDAO();
			tripDAO.saveStationOut(ticket24h.getID(), idStation);
		}
		return "OK";
	}
}
