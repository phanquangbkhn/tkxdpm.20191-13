package com.tkxdpm.entity;

import java.sql.Timestamp;

import com.tkxdpm.dao.TicketOnewayDAO;

public class TicketOneway extends Entity {
	private String stationIn;
	private String stationOut;
	private double price;
	
	public TicketOneway() {}
	public TicketOneway(String id, String code, String status, double price, String stationIn, String stationOut, Timestamp createAt) {
		super(id, code, status, createAt);
		this.stationIn = stationIn;
		this.stationOut = stationOut;
		this.price = price;
	}
	
	public String getStationIn() {
		return stationIn;
	}
	public void setStationIn(String stationIn) {
		this.stationIn = stationIn;
	}
	
	public String getStationOut() {
		return stationOut;
	}
	public void setStationOut(String stationOut) {
		this.stationOut = stationOut;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	@Override
	public String toString() {
		String result = "Ticket-Oneway " + "between " + stationIn + " and " + stationOut;
		result += ". Status is " + this.getStatus() + " - " + this.getPrice() + " euros"; 
		return result;	
	}
	
	/**
	 * This function is change status of ticket to "in"
	 */
	@Override
	public void changeStatusIn() {
		TicketOnewayDAO toDAO = new TicketOnewayDAO();
		toDAO.changeStatus(this.id , this.status);
	}
	
	/**
	 * This function is change status of ticket to "destroy"
	 */
	@Override
	public void changeStatusOut() {
		TicketOnewayDAO toDAO = new TicketOnewayDAO();
		toDAO.changeStatus(this.id , this.status);
	}
}
