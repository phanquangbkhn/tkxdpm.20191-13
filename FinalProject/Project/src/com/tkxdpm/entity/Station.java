package com.tkxdpm.entity;

public class Station {
	private int id;
	private String name;
	private double location;
	
	public Station(int id, String name, double location) {
		this.id = id;
		this.name = name;
		this.location = location;
	}
	
	public int getID() {
		return id;
	}
	public void setID(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public double getLocation() {
		return location;
	}
	public void setLocation(double location) {
		this.location = location;
	}
}
