package com.tkxdpm.entity;

import java.sql.Timestamp;

public abstract class Entity {
	protected String id;
	protected String code;
	protected String status;
	protected Timestamp createAt;
	
	public Entity() {}
	public Entity(String id, String code, String status, Timestamp createAt) {
		this.id = id;
		this.code = code;
		this.status = status;
		this.createAt = createAt;
	}
	
	public String getID() {
		return id;
	}
	public void setID(String id) {
		this.id = id;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public Timestamp getCreateAt() {
		return createAt;
	}
	public void setCreateAt(Timestamp createAt) {
		this.createAt = createAt;
	}
	
	@Override
	public abstract String toString();
	public abstract void changeStatusIn();
	public abstract void changeStatusOut();
}
