package com.tkxdpm.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.tkxdpm.entity.Ticket24h;

public class Ticket24hDAO {

	/**
	 * This function is used to get one Ticket24h Object from it's code
	 * 
	 * @param code Code of Ticket24h
	 * @return Ticket24h One instance of Ticket 24h class
	 */
	public Ticket24h getTicket24hByCode(String code) {
		Ticket24h ticket24h = new Ticket24h();
		try {
			Connection conn = DBConnector.getInstance().getConnection();
			String sqlString = "select * from ticket_24h where code=\"" + code + "\"";
			Statement stm = conn.createStatement();
			ResultSet rs = stm.executeQuery(sqlString);
			if (!rs.next()) {
				return null;
			} else {
				do {
					ticket24h.setID(rs.getString(1));
					ticket24h.setCode(rs.getString(2));
					ticket24h.setStatus(rs.getString(3));
					ticket24h.setPrice(rs.getDouble(4));
					ticket24h.setFirstTimeUse(rs.getTimestamp(5));
					ticket24h.setCreateAt(rs.getTimestamp(6));
				} while (rs.next());
			}
		} catch (SQLException e) {
			System.out.println("Error StationDAO");
			return null;
		}
		return ticket24h;
	}

	/**
	 * This method change 24 ticket's status
	 * @param ticketID This is ticket id of ticket you want to change status
	 * @param status This is status you want to change
	 */
	public void changeStatus(String ticketID, String status) {
		try {
			Connection conn = DBConnector.getInstance().getConnection();
			String sqlString = "update ticket_24h set status=\"" + status + "\"";
			sqlString += " where id = \"" + ticketID + "\"";
			Statement stm = conn.createStatement();
			stm.executeUpdate(sqlString);
		} catch (SQLException e) {
			System.out.println("Error Ticket24hDAO update");
		}
	}

	/**
	 * This function is used to update the first-time use of Ticket24h
	 * 
	 * @param ticket24h Instance of Ticket24h class
	 */
	public void updateFirstTimeUse(Ticket24h ticket24h) {
		try {
			Connection conn = DBConnector.getInstance().getConnection();
			String sqlString = "update ticket_24h set first_time_use=";
			sqlString += "'" + ticket24h.getFirstTimeUse() + "'";
			sqlString += " where id = \"" + ticket24h.getID() + "\"";
			Statement stm = conn.createStatement();
			stm.executeUpdate(sqlString);
		} catch (SQLException e) {
			System.out.println("Error! Cannot update 24h ticket status!");
		}
	}
}
