package com.tkxdpm.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnector {
	private static final String className = "com.mysql.cj.jdbc.Driver";
	private static final String url = "jdbc:mysql://localhost:3306/afc?serverTimezone=UTC"
			+ "&useUnicode=yes&characterEncoding=UTF-8&zeroDateTimeBehavior=convertToNull";
	private static final String user = "root";
	private static final String pass = "";
	private Connection connection;
	private static DBConnector instance;

	/**
	 * This method connect to database
	 * 
	 * @throws SQLException
	 */
	private DBConnector() throws SQLException {
		try {
			Class.forName(className);
			this.connection = DriverManager.getConnection(url, user, pass);
		} catch (ClassNotFoundException ex) {
			System.out.println("Database Connection Creation Failed : " + ex.getMessage());
		}
	}

	/**
	 * 
	 * @return Connection This return connection to database
	 */
	public Connection getConnection() {
		return connection;
	}

	/**
	 * This method will check if the object already exists, if not then instantiate
	 * the object, otherwise return the unique object of class DBConnector.
	 * 
	 * @return instance of DBConnector
	 * @throws SQLException throws SQL exception 
	 */
	public static DBConnector getInstance() throws SQLException {
		if (instance == null) {
			instance = new DBConnector();
		} else if (instance.getConnection().isClosed()) {
			instance = new DBConnector();
		}
		return instance;
	}

	/**
	 * Close connection
	 */
	public void close() {
		try {
			this.connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}