package com.tkxdpm.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import com.tkxdpm.entity.Trip;

public class TripDAO {
	private Connection conn;
	
	/**
	 * This function is used to insert one trip to database
	 * @param trip Instance of Trip class
	 * @param idTicketCard Index of Ticket or Card
	 * @param idStation Index of the station input
	 */
	public void insertTrip(Trip trip, String idTicketCard, int idStation) {
		try {
			Connection conn = DBConnector.getInstance().getConnection();
			String sqlString = "insert into trips values ";
			sqlString += "(" + trip.getID() + "," + "'" + idTicketCard + "'" + "," + idStation;
			sqlString += "," + "null" + "," + "'" + trip.getStationInTime();
			sqlString += "'" + "," + "null" + ")";
			Statement stm = conn.createStatement();
			stm.executeUpdate(sqlString);
		} catch (SQLException e) {
			System.out.println("Error Trip DAO insert");
		}
	}
	/**
	 * This function is used to get max index of trip from table "trips"
	 * @return integer Max ID from table "trips"
	 */
	public int getMaxID() {
		int id = 0;
		try {
			Connection conn = DBConnector.getInstance().getConnection();
			String sqlString = "select max(id) from trips";
			Statement stm = conn.createStatement();
			ResultSet rs = stm.executeQuery(sqlString);
			if (rs.next()) {
				return rs.getInt(1);
			}

		} catch (SQLException e) {
			System.out.println("Error Trip DAO get max");
		}
		return id;
	}
	
	/**
	 * This method gets trip id with ticket or card id 
	 * @param tcID: ticket or card id
	 * @return: station in real's id
	 */
	public int getStationInRealID(String tcID) {
		int stationInReal = 0;
		String sql = "SELECT stationInReal FROM afc.trips where tc_id = \"" + tcID
				+ "\" and stationInReal IS NOT NULL and stationOutReal IS NULL;";
		PreparedStatement ps = null;
		Connection conn = null;
		try {
			conn = DBConnector.getInstance().getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				stationInReal = rs.getInt("stationInReal");
			}
		} catch (SQLException ex) {
			System.out.println(ex);
		}
		return stationInReal;
	}
	/**
	 * This method gets current trip's id 
	 * @param tcID This is current ticket or card's id
	 * @return int This return current trip's id
	 */
	private int getCurrentTripID(String tcID) {
		String sqlGetTripID = "SELECT id FROM trips where tc_id = \"" + tcID
				+ "\" and stationOutReal is null and stationInReal is not null;";
		PreparedStatement ps = null;
		try {
			conn = DBConnector.getInstance().getConnection();
			ps = conn.prepareStatement(sqlGetTripID);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				return rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}
	/**
	 * This method is get id current trip and save location trip
	 * @param tcID This is current ticket or card's id
	 * @param stationOutReal This is station out real's id
	 */
	public void saveStationOut(String tcID, int stationOutReal) {
		int tripID = getCurrentTripID(tcID);
		Timestamp currentTime = new Timestamp(System.currentTimeMillis());
		String dateFormatString = "yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormatString);
		String stationOutTime = simpleDateFormat.format(currentTime.getTime());
		
		String sqlUpdate = "UPDATE trips SET stationOutReal = " + stationOutReal + ", stationOutTime = '"
				+ stationOutTime + "' WHERE (id = '" + tripID + "');";
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(sqlUpdate);
			int result = ps.executeUpdate();
			if (result != 0) {
//				System.out.println("test update trip ok!");
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
}
