package com.tkxdpm.main;

import java.util.Scanner;
import com.tkxdpm.boundary.BListStation;

public class Main {
	public static void main(String[] args) {
		String loop;
		Scanner scanner = new Scanner(System.in);
		do {
			BListStation bListStation = new BListStation();
			bListStation.printListStation();
			bListStation.execute();
			System.out.println("Please enter any key to continue or 1 to exit ...");
			loop = scanner.nextLine();
		} while (!loop.equals("1"));
		System.out.println("Finish program!");
		scanner.close();
	}	
}
