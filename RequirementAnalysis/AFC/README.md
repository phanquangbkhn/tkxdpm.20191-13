# Bài tập Homework 01:
# Hệ thống soát vé tự động Automated Fare Controller (AFC)

## Phân chia công việc:
|                          Công việc                                       |     Họ tên        |
|--------------------------------------------------------------------------|-------------------|
| Đặc tả Usecase Kiểm vé ra, đặc tả phụ trợ Usability - Reliability        | Lê Đình Phúc      |
| Đặc tả Usecase Kiểm vé vào, đặc tả phụ trợ Functionality                 | Nguyễn Anh Phương |
| Đặc tả Usecase Kiểm thẻ vào, đặc tả phụ trợ Performance - Supportability | Phan Văn Quang    |
| Đặc tả Usecase Kiểm thẻ ra, từ điển thuật ngữ                            | Phạm Hồng Quân    |


## Phân chia công việc review:

|   Họ và tên             |     Người review |
|------------------------ | ---------------  |
| Lê Đình Phúc            | Phạm Hồng Quân   |
| Nguyễn Anh Phương       | Lê Đình Phúc     |
| Phan Văn Quang          | Nguyễn Anh Phương|
| Phạm Hồng Quân          | Phan Văn Quang   |