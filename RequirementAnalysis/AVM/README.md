# Bài tập Homework 01:

## Phân chia công việc:
|   Công việc                                      |      Họ tên       |
|--------------------------------------------------|:--------------:   |
| Luồng sự kiện Mua thẻ, Mua vé                    | Phan Văn Quang    |
| Luồng sự kiện Thanh toán              | Lê Đình Phúc      |
| Luồng sự kiện Xuất vé, Xuất thẻ                  | Phạm Hồng Quân    |
| Luồng sự kiện Xem chi tiết thẻ, Nạp tiền vào thẻ | Nguyễn Anh Phương |

## Phân chia công việc review:

|   Họ và tên             |     Người review |
|------------------------ |:--------------:  |
| Nguyễn Anh Phương       | Phạm Hồng Quân   |
| Lê Đình Phúc            | Nguyễn Anh Phương|
| Phan Văn Quang          | Lê Đình Phúc     |
| Phạm Hồng Quân          | Phan Văn Quang   |