﻿﻿﻿﻿# Bài tập Homework 03: Thiết kế chi tiết

## Phân chia công việc:
### Công việc chung:
- Tạo biểu đồ di chuyển màn hình 
- Tổng hợp báo cáo

### Công việc riêng
|                          Công việc                                       |     Họ tên        |
|--------------------------------------------------------------------------|-------------------|
| Vẽ giao diện Kiểm vé ra 1 chiều và 24h   	                               | Lê Đình Phúc      |
| Vẽ giao diện Kiểm vé vào 1 chiều và 24h                                  | Nguyễn Anh Phương |
| Vẽ giao diện Kiểm thẻ vào                                                | Phan Văn Quang    |
| Vẽ giao diện Kiểm thẻ ra                                                 | Phạm Hồng Quân    |

### Phân nhóm nhỏ
|                          Công việc                                      |            Họ tên                 |
|-------------------------------------------------------------------------|-----------------------------------|
| Biểu đồ lớp thiết kế và biểu đồ lớp chi tiết                            | Nguyễn Anh Phương & Phạm Hồng Quân|
| Thiết kế dữ liệu: biểu đồ thực thể liên kết, chuẩn hóa và thiết kế CSDL | Lê Đình Phúc & Phan Văn Quang     |


## Phân chia công việc review:

|   Họ và tên             |     Người review  |
|------------------------ |:----------------: |
| Lê Đình Phúc            | Nguyễn Anh Phương |
| Nguyễn Anh Phương       | Phan Văn Quang    |
| Phan Văn Quang          | Phạm Hồng Quân    |
| Phạm Hồng Quân          | Lê Đình Phúc      |