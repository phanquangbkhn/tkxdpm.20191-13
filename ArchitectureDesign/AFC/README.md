﻿﻿﻿# Bài tập Homework 02:

## Phân chia công việc:
### Công việc chung:
- Sửa bài tập homework 01
- Vẽ biểu đồ lớp tổng quan
### Công việc riêng
|                          Công việc                                       |     Họ tên        |
|--------------------------------------------------------------------------|-------------------|
| Vẽ biểu tương tác, biểu đồ lớp UC Kiểm vé ra 1 chiều và 24h   		   | Lê Đình Phúc      |
| Vẽ biểu tương tác, biểu đồ lớp UC Kiểm vé vào 1 chiều và 24h             | Nguyễn Anh Phương |
| Vẽ biểu tương tác, biểu đồ lớp UC Kiểm thẻ vào                           | Phan Văn Quang    |
| Vẽ biểu tương tác, biểu đồ lớp UC Kiểm thẻ ra                            | Phạm Hồng Quân    |

## Phân chia công việc review:

|   Họ và tên             |     Người review |
|------------------------ |:--------------:  |
| Nguyễn Anh Phương       | Phạm Hồng Quân   |
| Lê Đình Phúc            | Nguyễn Anh Phương|
| Phan Văn Quang          | Lê Đình Phúc     |
| Phạm Hồng Quân          | Phan Văn Quang   |