package ticket;

import java.sql.Timestamp;
import org.junit.jupiter.api.Test;
import com.tkxdpm.controller.Ticket24hController;
import com.tkxdpm.entity.Ticket24h;
import com.tkxdpm.utils.DateFormat;

import static org.junit.jupiter.api.Assertions.assertEquals;
public class TestCheckoutTicket24h {
	
		@Test
		void checkoutTest() {
			Ticket24hController ticket24hController;;
			Timestamp createAt = new DateFormat().parsingStringToTimestamp("2019-11-09 07:16:09");
			Ticket24h fakeTicket24h = null;
			int fakeIdStation = 1;
			Timestamp fakeFirstTimeUse = null;
	
			// Error! Status is new
			String fakeStatusNew = "new";
			fakeTicket24h = new Ticket24h("TF201910260002", "ea9d3d1e279e8fd6", fakeStatusNew, 8.5, fakeFirstTimeUse, createAt);
			ticket24hController = new Ticket24hController(fakeTicket24h, fakeIdStation);
			assertEquals("The ticket's status is \"New\"!" + "\nYou must checkin your ticket before checkout it.",
					ticket24hController.checkOut());
			
			//Error! Status in out
			String fakeStatusOut = "out";
			fakeFirstTimeUse  = new DateFormat().parsingStringToTimestamp("2019-11-09 08:16:09");
			fakeTicket24h = new Ticket24h("TF201912080003", "bc179d76a1e1d1a5", fakeStatusOut, 8.5, fakeFirstTimeUse, createAt);
			ticket24hController = new Ticket24hController(fakeTicket24h, fakeIdStation);
			assertEquals("The ticket's status is \"Out station\"!" + "\nYou must checkin your ticket before checkout it.",
					ticket24hController.checkOut());
			
			//Error! Status in destroy
			String fakeStatusDestroy = "destroy";
			fakeFirstTimeUse  = new DateFormat().parsingStringToTimestamp("2019-11-09 08:16:09");
			fakeTicket24h = new Ticket24h("TF201910260001", "6d3f33e7ac1f9cbb", fakeStatusDestroy, 8.5, fakeFirstTimeUse, createAt);
			ticket24hController = new Ticket24hController(fakeTicket24h, fakeIdStation);
			assertEquals("The ticket's status is \"Destroy\"!",
					ticket24hController.checkOut());
			
			//Valid status is in
			String fakeStatusIn = "in";
			fakeFirstTimeUse  = new DateFormat().parsingStringToTimestamp("2019-11-09 08:16:09");
			fakeTicket24h = new Ticket24h("TF201912080004", "2a2c3b88bcf97a5c", fakeStatusIn, 8.5, fakeFirstTimeUse, createAt);
			ticket24hController = new Ticket24hController(fakeTicket24h,fakeIdStation);
			assertEquals("OK",
					ticket24hController.checkOut());
		}
}
