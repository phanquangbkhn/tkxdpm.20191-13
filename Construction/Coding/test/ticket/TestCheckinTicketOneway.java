package ticket;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.Timestamp;

import org.junit.Before;
import org.junit.Test;


import com.tkxdpm.controller.TicketOnewayController;

import com.tkxdpm.entity.TicketOneway;
import com.tkxdpm.utils.DateFormat;

public class TestCheckinTicketOneway {
	private TicketOnewayController ticketOnewayC;
	private TicketOneway ticketOneway;
	@Before 
	public void setUp() {
		ticketOnewayC = new TicketOnewayController();
		ticketOneway = new TicketOneway();
		ticketOnewayC.setTicketOneway(ticketOneway);
	}
	@Test 
	public void testCheckStatusIn() {
		ticketOneway.setStatus("new");
		assertEquals(null, ticketOnewayC.checkStatusIn());

		ticketOneway.setStatus("in");
		assertEquals("Ticket's status is \"In\"" + "!!! Please choose other ticket!", ticketOnewayC.checkStatusIn());
		
		ticketOneway.setStatus("destroy");
		assertEquals("Ticket's status is \"Destroy\"" + "!!! Please choose other ticket!", ticketOnewayC.checkStatusIn());
		
		ticketOneway.setStatus("huy");
		assertEquals("Ticket's status is \"Destroy\"" + "!!! Please choose other ticket!", ticketOnewayC.checkStatusIn());
			
	}
	
	@Test
	public void testCheckStationInReal() {
		
		ticketOneway.setStationIn("Saint-Lazare");
		ticketOneway.setStationOut("Madeleinne");
		ticketOnewayC.setIDStation(1);
		assertTrue(ticketOnewayC.checkStationInReal());
		
		ticketOneway.setStationIn("Saint-Lazare");
		ticketOneway.setStationOut("Chatelet");
		ticketOnewayC.setIDStation(4);
		assertTrue(ticketOnewayC.checkStationInReal());
		
		
	}
	
	@Test
	public void testcheckIn() {

		ticketOneway.setID("TF201910260002");
		ticketOneway.setCode("ea9d3d1e279e8fd6");
		ticketOneway.setPrice(8.5);
		ticketOneway.setStationIn("Saint-Lazare");
		ticketOneway.setStationOut("Madeleinne");
		
		
		//test status new
		ticketOneway.setStatus("new");
		ticketOnewayC.setTicketOneway(ticketOneway);
		ticketOnewayC.setIDStation(1);
		assertEquals("OK", ticketOnewayC.checkIn());
		
				
		
		// test status in
		ticketOneway.setStatus("in");
		ticketOnewayC.setTicketOneway(ticketOneway);
		ticketOnewayC.setIDStation(1);
		assertEquals("Ticket's status is \"In\"" + "!!! Please choose other ticket!", ticketOnewayC.checkIn());
		
		// test status destroy
		
		ticketOneway.setStatus("destroy");
		ticketOnewayC.setTicketOneway(ticketOneway);
		ticketOnewayC.setIDStation(1);
		assertEquals("Ticket's status is \"Destroy\"" + "!!! Please choose other ticket!", ticketOnewayC.checkIn());
		
	}
}

