package ticket;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.Timestamp;

import org.junit.Before;
import org.junit.Test;


import com.tkxdpm.controller.Ticket24hController;

import com.tkxdpm.entity.Ticket24h;
import com.tkxdpm.utils.DateFormat;

public class TestCheckinTicket24h {
	private Ticket24hController ticket24hC;
	private Ticket24h ticket24h;
	@Before 
	public void setUp() {
		ticket24hC = new Ticket24hController();
		ticket24h = new Ticket24h();
		ticket24hC.setTicket24h(ticket24h);
	}
	@Test 
	public void testCheckStatusIn() {
		ticket24h.setStatus("new");
		assertEquals(null, ticket24hC.checkStatusIn());

		ticket24h.setStatus("out");
		assertEquals(null, ticket24hC.checkStatusIn());
		
		ticket24h.setStatus("in");
		assertEquals("Ticket's status is \"In\"" + "!!! Please choose other ticket!", ticket24hC.checkStatusIn());
		
		ticket24h.setStatus("destroy");
		assertEquals("Ticket's status is \"Destroy\"" + "!!! Please choose other ticket!", ticket24hC.checkStatusIn());
		
		ticket24h.setStatus("huy");
		assertEquals("Ticket's status is \"Destroy\"" + "!!! Please choose other ticket!", ticket24hC.checkStatusIn());
			
	}
	
	@Test
	public void testCheckStatusNew() {
		ticket24h.setStatus("new");
		assertTrue(ticket24hC.checkStatusNew());
		
		ticket24h.setStatus("old");
		assertFalse(ticket24hC.checkStatusNew());
		
		ticket24h.setStatus("0");
		assertFalse(ticket24hC.checkStatusNew());
	}
	
	@Test
	public void testcheckIn() {
		Timestamp createAt = new DateFormat().parsingStringToTimestamp("2019-11-09 07:16:09");
		ticket24h.setID("TF201910260002");
		ticket24h.setCode("ea9d3d1e279e8fd6");
		ticket24h.setPrice(8.5);
		ticket24h.setFirstTimeUse(createAt);
		
		//test status new
		ticket24h.setStatus("new");
		ticket24hC.setTicket24h(ticket24h);
		ticket24hC.setIDStation(1);
		assertEquals("OK", ticket24hC.checkIn());
		
		
		// test status out
		ticket24h.setStatus("out");
		ticket24hC.setTicket24h(ticket24h);
		ticket24hC.setIDStation(1);
		assertEquals("OK", ticket24hC.checkIn());
		
		
		// test status in
		ticket24h.setStatus("in");
		ticket24hC.setTicket24h(ticket24h);
		ticket24hC.setIDStation(1);
		assertEquals("Ticket's status is \"In\"" + "!!! Please choose other ticket!", ticket24hC.checkIn());
		
		// test status destroy
		
		ticket24h.setStatus("destroy");
		ticket24hC.setTicket24h(ticket24h);
		ticket24hC.setIDStation(1);
		assertEquals("Ticket's status is \"Destroy\"" + "!!! Please choose other ticket!", ticket24hC.checkIn());
		
	}
}

