package ticket;

import java.sql.Timestamp;
import org.junit.jupiter.api.Test;
import com.tkxdpm.controller.TicketOnewayController;
import com.tkxdpm.entity.TicketOneway;
import com.tkxdpm.utils.DateFormat;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestCheckoutTicketOneway {

	@Test
	void checkoutTest() {
		TicketOnewayController ticketOnewayController;
		Timestamp creatAt = new DateFormat().parsingStringToTimestamp("2019-11-10 07:16:09");
		TicketOneway fakeTicketOneway = null;
		// Station in real id = 3;
		int fakeStationOutRealID = 5;
		
		// Error! Status is New
		String fakeStatusNew = "new";
		fakeTicketOneway = new TicketOneway("OW201910261000", "edcd20bc35ae4b0b", fakeStatusNew, 1.9, "Saint-Lazare",
				"Pyramides", creatAt);
		ticketOnewayController = new TicketOnewayController(fakeTicketOneway, fakeStationOutRealID);
		assertEquals("The ticket's status is \"New\". You can check in before check out station.",
				ticketOnewayController.checkOut());

		
		// Error! Status is Out
		String fakeStatusDestroy = "destroy";
		fakeTicketOneway = new TicketOneway("OW201910261001", "4594bb26f2f93c5c", fakeStatusDestroy, 1.9, "Saint-Lazare",
				"Gare de Lyon", creatAt);
		ticketOnewayController = new TicketOnewayController(fakeTicketOneway, fakeStationOutRealID);
		assertEquals("The ticket's status is \"Destroy\".",
				ticketOnewayController.checkOut());

		
		// Error! Not enough balance (Status is in)
		String fakeStatusIn = "in";
		fakeStationOutRealID = 9; // priceReal = 2.5 euros
		fakeTicketOneway = new TicketOneway("OW201910261002", "e59995de368c2adf", fakeStatusIn, 2.7, "Saint-Lazare",
				"Cour Saint-Emilion", creatAt);
		ticketOnewayController = new TicketOnewayController(fakeTicketOneway, 1);
		assertEquals("Not enough balance: Expected " + 2.7 + " euros.",
				ticketOnewayController.checkOut());

		
		// Valid ticket: Status is in and enough price
		fakeStationOutRealID = 5; // priceReal = 2.5 erous
		fakeTicketOneway = new TicketOneway("OW201910261003", "236304c2a3e505cb", "in", 3.1, "Saint-Lazare",
				"Bibliotheque Francois Mitterrand", creatAt);
		ticketOnewayController = new TicketOnewayController(fakeTicketOneway, fakeStationOutRealID);
		assertEquals("OK", ticketOnewayController.checkOut());
	}

}
