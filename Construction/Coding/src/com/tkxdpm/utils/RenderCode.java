package com.tkxdpm.utils;

import java.util.ArrayList;

import hust.soict.se.customexception.InvalidIDException;
import hust.soict.se.recognizer.TicketRecognizer;
import hust.soict.se.scanner.CardScanner;

public class RenderCode {
	/**
	 * This function is used to convert list barcode to list code
	 * @param listFilterBarcode List barcode is after being filtered from file input
	 * @return ArrayList{@literal <}String{@literal >} One list string array, One element is a code string
	 */
	public ArrayList<String> renderCode(ArrayList<String> listFilterBarcode) {
		ArrayList<String> listCode = new ArrayList<String>();
		for(String barcode : listFilterBarcode) {
			if(Character.isUpperCase(barcode.charAt(0))) {
				CardScanner cardScanner = CardScanner.getInstance();
				try {
					String cardCode = cardScanner.process(barcode);
					listCode.add(cardCode);
				} catch (InvalidIDException e) {
					e.printStackTrace();
				}
			}
			else if(Character.isLowerCase(barcode.charAt(0))) {
				TicketRecognizer ticketRecognier = TicketRecognizer.getInstance();
				try {
					String ticketCode = ticketRecognier.process(barcode);
					listCode.add(ticketCode);
				} catch (InvalidIDException e) {
					e.printStackTrace();
				}
			}
		}
		return listCode;
	}

	/**
	 * This function is used to render barcode to code of ticket
	 * @param barcode Barcode is entered from keyboard
	 * @return String Code of ticket
	 */
	public String renderTicketCode(String barcode) {
		try {
			TicketRecognizer ticketRecognier = TicketRecognizer.getInstance();
			String ticketCode = ticketRecognier.process(barcode);
			return ticketCode;
		} catch (InvalidIDException e) {
			e.printStackTrace();
		}
		return null;
	}


	/**
	 * This function is used to render barcode to code of card
	 * @param barcode Barcode is entered from keyboard
	 * @return String Code of card
	 */
	public String renderCardCode(String barcode) {
		String cardCode = null;
		try {
			CardScanner cardScanner = CardScanner.getInstance();
			cardCode = cardScanner.process(barcode);
			
		} catch (InvalidIDException e) {
			e.printStackTrace();
		}
		return cardCode;
	}
}
