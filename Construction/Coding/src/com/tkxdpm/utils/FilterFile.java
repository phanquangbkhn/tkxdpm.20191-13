package com.tkxdpm.utils;

import java.util.ArrayList;


public class FilterFile {
	/**
	 * This function is used to filter list barcode from file input
	 * @param listBarcode one array list barcode string from file input
	 * @return ArrayList{@literal <}String{@literal >} one array list barcode is after being filtered
	 */
	public ArrayList<String> filterFile(ArrayList<String> listBarcode) {
		ArrayList<String> listFilterBarcode = new ArrayList<String>();
		for(String barcode : listBarcode) {
			int c = barcode.length();
			if(CheckUpperString.checkUpperString(barcode) && (c == 8)) {
				listFilterBarcode.add(barcode);
			}
			else if(CheckLowerString.checkLowerString(barcode) && (c == 8)) {
				listFilterBarcode.add(barcode);
			}
		}
		
		return listFilterBarcode;	
	}
}
