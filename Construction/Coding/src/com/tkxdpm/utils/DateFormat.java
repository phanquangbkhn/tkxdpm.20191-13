package com.tkxdpm.utils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DateFormat {
	/**
	 * This method format time to String E, HH:mm - dd MMM yyyy
	 * @param time This is milliseconds time
	 * @return String This return time string format
	 */
	public String dateFormString(long time) {
		String dateFormatString = "E, HH:mm - dd MMM yyyy";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormatString);
		return simpleDateFormat.format(time);
	}
	
	/**
	 * This method is parsing String to Timestamp
	 * @param dateString This is time string yyyy-MM-dd hh:mm:ss
	 * @return Timestamp This return timestamp parsing
	 */
	public Timestamp parsingStringToTimestamp(String dateString) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String dateInString = dateString;
		Timestamp timestamp = null;
		try {
			timestamp = new Timestamp((sdf.parse(dateInString).getTime()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return timestamp;
	}
}
