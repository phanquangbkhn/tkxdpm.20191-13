package com.tkxdpm.boundary;

import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;

import com.tkxdpm.controller.AFCController;
import com.tkxdpm.entity.Entity;
import com.tkxdpm.entity.Station;

public class BListStation {

	private Scanner scanner;

	/**
	 * This function is used to print all stations
	 */
	public void printListStation() {
		System.out.println("1. Main Screen\n\n");
		System.out.println("There are stations in the line M14 of Paris:\n");
		// Goi den controller station de hien thị len danh sach station
		AFCController afcController = new AFCController();
		ArrayList<Station> listStation = afcController.getAllStation();
		char i = 'a';
		for (Station station : listStation) {
			System.out.println(i + ". " + station.getName() + station.getID());
			i++;
		}
		System.out.println("\nAvailable actions: 1-enter station, 2-exit station\n\n");
		System.out.println("You can provide a combination of number (1 or 2) "
				+ "and a letter from (a to i) to enter or exit\na station "
				+ "(using hyphen in between). For instance, the combination \"2-d\" "
				+ "will bring you to\nexit the station: ");
	}
	
	/**
	 * This function is used to check input
	 * 
	 * @param input Input from keyboard
	 * @return boolean If input is the from "1-a" return true, else return false
	 */
	public boolean checkInput(String input) {
		if (input.length() != 3) {
			return false;
		}
		Character c1 = input.charAt(0);
		if (c1 != '1' && c1 != '2') {
			return false;
		}
		Character c2 = input.charAt(1);
		if (c2 != '-') {
			return false;
		}
		Character c3 = input.charAt(2);
		if (c3 >= 'a' && c3 <= 'z') {
			return true;
		}
		return false;
	}

	public void execute(){
		String exitOrEnterStation = null;
		scanner = new Scanner(System.in);
		do {
			exitOrEnterStation = scanner.nextLine();
			if (checkInput(exitOrEnterStation) == false) {
				System.out.println("Input error! Please enter your input again!");
			}
		} while (checkInput(exitOrEnterStation) == false);
		// Goi den lop bien khac de tiep tuc chuong trinh
		// lay ra ky tu dau tien la 1 hay 2
		
		Character exitOrEnter = exitOrEnterStation.charAt(0);
		int indexStation = exitOrEnterStation.codePointAt(2) - 96;
		
		AFCController afcController = new AFCController();
		Map<String, Entity> mapBarcodeEntity =  afcController.getMapEntityByBarcode();
		BListTicketCard bListTicketCard = new BListTicketCard(exitOrEnter, indexStation);
		bListTicketCard.printListTicketCard(mapBarcodeEntity);
		bListTicketCard.handleBarcode();
	}	
}
