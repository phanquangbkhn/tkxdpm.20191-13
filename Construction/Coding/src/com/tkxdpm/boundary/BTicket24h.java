package com.tkxdpm.boundary;

import com.tkxdpm.entity.Ticket24h;
import com.tkxdpm.utils.DateFormat;

public class BTicket24h {
	/**
	 * This method is print check result
	 * 
	 * @param ticket24h This is information of 24h ticket object
	 * @param message   String This is an error message if any
	 */
	public void printResultTicket24h(Ticket24h ticket24h, String message) {
		long timeUse = 0;
		boolean checkFirstTimeUse = !ticket24h.getStatus().equals("new");
		if (checkFirstTimeUse) {
			timeUse = ticket24h.getFirstTimeUse().getTime() + 24 * 60 * 60 * 1000;
		}
		System.out.println("Type: 24h-Ticket");
		System.out.println("ID: " + ticket24h.getID());
		System.out.println("Price: " + ticket24h.getPrice() + "$");

		if (message.equals("OK")) {
			if (checkFirstTimeUse) {
				System.out.println("Valid for 24 hours from: " + new DateFormat().dateFormString(timeUse));
			}
		} else {
			if (checkFirstTimeUse) {
				System.out.println("Exprired at: " + new DateFormat().dateFormString(timeUse));
			}
			System.out.println(message);
		}
	}
}
