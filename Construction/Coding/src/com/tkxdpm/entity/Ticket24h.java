package com.tkxdpm.entity;

import java.sql.Timestamp;

import com.tkxdpm.dao.Ticket24hDAO;

public class Ticket24h extends Entity {
	private Timestamp firstTimeUse;
	private double price;
	
	public Ticket24h() {}
	public Ticket24h(String id, String code, String status, double price, Timestamp firstTimeUse, Timestamp createAt) {
		super(id, code, status, createAt);
		this.firstTimeUse = firstTimeUse;
		this.price = price;
	}
	
	public Timestamp getFirstTimeUse() {
		return firstTimeUse;
	}
	
	public void setFirstTimeUse(Timestamp firstTimeUse) {
		this.firstTimeUse = firstTimeUse;
	}
	
	public double getPrice() {
		return price;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	@Override
	public String toString() {
		String result = "24h Ticket. Status is " + this.getStatus() + " ";
		if(this.getFirstTimeUse() != null) {
			result += "- Until " + this.getFirstTimeUse();
		}
		return result;
	}
	
	/**
	 * This function is change status of ticket to "in"
	 */
	@Override
	public void changeStatusIn() {
		Ticket24hDAO t24hDAO = new Ticket24hDAO();
		t24hDAO.changeStatus(this.id, this.status);
	}
	
	/**
	 * This function is change status of ticket out or destroy
	 */
	@Override
	public void changeStatusOut() {
		Ticket24hDAO t24hDAO = new Ticket24hDAO();
		t24hDAO.changeStatus(this.id, this.status);
	}
}
