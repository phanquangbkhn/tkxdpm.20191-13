package com.tkxdpm.entity;

import java.sql.Timestamp;

public class Card extends Entity {
	private double balance;
	private Timestamp latestTimeIn;
    private String stationIn;
    private String stationOut;


    public Card() {}
	public Card(String id, String code, String status, double balance, Timestamp createAt, Timestamp latestTimeIn, String stationIn, String stationOut) {
		super(id, code, status, createAt);
		this.balance = balance;
		this.latestTimeIn = latestTimeIn;
		this.stationIn = stationIn;
		this.stationOut = stationOut;
	}
	
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	
	public Timestamp getLatestTimeIn() {
		return latestTimeIn;
	}

	public void setLatestTimeIn(Timestamp latestTimeIn) {
		this.latestTimeIn = latestTimeIn;
	}

    public String getStationIn() {
        return stationIn;
    }

    public void setStationIn(String stationIn) {
        this.stationIn = stationIn;
    }

    public String getStationOut() {
        return stationOut;
    }

    public void setStationOut(String stationOut) {
        this.stationOut = stationOut;
    }

    public String toString() {
		String result = "Prepaid Card: " + balance + " euros ";
		result += "Status is "+ this.status;
		return result;
	}
	@Override
	public void changeStatusIn() {
		// TODO Auto-generated method stub
	}
	@Override
	public void changeStatusOut() {
		// TODO Auto-generated method stub
	}
}
