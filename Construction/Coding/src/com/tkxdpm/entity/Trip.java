package com.tkxdpm.entity;

import java.sql.Timestamp;

public class Trip {
	private int id;
	private String stationInReal;
	private String stationOutReal;
	private Timestamp stationInTime;
	private Timestamp stationOutTime;
	
	public Trip() {
		
	}
	public Trip(int id, String stationInReal, String stationOutReal, Timestamp stationInTime, Timestamp stationOutTime) {
		this.setID(id);
		this.setStationInReal(stationInReal);
		this.setStationOutReal(stationOutReal);
		this.setStationInTime(stationInTime);
		this.setStationOutTime(stationOutTime);
	}
	public int getID() {
		return id;
	}
	public void setID(int id) {
		this.id = id;
	}
	public String getStationInReal() {
		return stationInReal;
	}
	public void setStationInReal(String stationInReal) {
		this.stationInReal = stationInReal;
	}
	/**
	 * @return the stationOutReal
	 */
	public String getStationOutReal() {
		return stationOutReal;
	}
	/**
	 * @param stationOutReal the stationOutReal to set
	 */
	public void setStationOutReal(String stationOutReal) {
		this.stationOutReal = stationOutReal;
	}
	/**
	 * @return the stationOutTime
	 */
	public Timestamp getStationOutTime() {
		return stationOutTime;
	}
	/**
	 * @param stationOutTime the stationOutTime to set
	 */
	public void setStationOutTime(Timestamp stationOutTime) {
		this.stationOutTime = stationOutTime;
	}
	/**
	 * @return the stationInTime
	 */
	public Timestamp getStationInTime() {
		return stationInTime;
	}
	/**
	 * @param stationInTime the stationInTime to set
	 */
	public void setStationInTime(Timestamp stationInTime) {
		this.stationInTime = stationInTime;
	}
}
