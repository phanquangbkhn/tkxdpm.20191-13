package com.tkxdpm.controller;

import java.sql.Timestamp;
import java.util.Date;
import com.tkxdpm.controller.caculatorPrice.CaculatorPrice;
import com.tkxdpm.controller.caculatorPrice.CaculatorPriceByDistanceOneLine;
import com.tkxdpm.dao.StationDAO;
import com.tkxdpm.dao.TripDAO;
import com.tkxdpm.entity.TicketOneway;
import com.tkxdpm.entity.Trip;

public class TicketOnewayController extends EntityController {
	private TicketOneway ticketOneway;
	public TicketOnewayController() {
		
	}
	public TicketOnewayController(TicketOneway ticketOneway, int idStation) {
		this.ticketOneway = ticketOneway;
		this.idStation = idStation;
	}
	public void setTicketOneway(TicketOneway ticketOneway) {
		this.ticketOneway = ticketOneway;
	}
	public void setIDStation(int idStation) {
		this.idStation = idStation;
	}
	/**
	 * This function is check status ticket when check in it
	 * @return error message if status is fail or null if it is ok
	 */
	public String checkStatusIn() {
		switch (ticketOneway.getStatus()) {
		case "new":
			return null;
		case "in":
			return "Ticket's status is \"In\"" + "!!! Please choose other ticket!";
		default:
			return "Ticket's status is \"Destroy\"" + "!!! Please choose other ticket!";
		}
	}


	/**
	 * This function is check station in real 
	 * @return true if station in real is between stationIn and stationOut in ticket, else return false
	 */
	public boolean checkStationInReal() {
		StationDAO stDAO = new StationDAO();
		String stationIn = ticketOneway.getStationIn();
		double locationIn = stDAO.getLocationByName(stationIn);
		String stationOut = ticketOneway.getStationOut();
		double locationOut = stDAO.getLocationByName(stationOut);
		double locationInReal = stDAO.getLocationByID(idStation);

		if (locationIn <= locationInReal && locationInReal <= locationOut) {
			return true;
		} else if (locationIn >= locationInReal && locationInReal >= locationOut) {
			return true;
		}
		return false;
	}

	/**
	 * This function is check in this one way ticket
	 */
	public String checkIn() {
		String statusMessage = checkStatusIn();
		if (statusMessage != null) {
			return statusMessage;
		} else if (checkStationInReal() == false) {
			return "Station is not valid";
		}
		ticketOneway.setStatus("in");
		ticketOneway.changeStatusIn();
		// generate trip and save it
		generateTrip();
		// open gate
		return "OK";
	}

	/**
	 * This function is used to generate new trip and save it
	 */
	public void generateTrip() {
		TripDAO tripDAO = new TripDAO();
		Trip trip = new Trip();
		// setID
		int idTrip = tripDAO.getMaxID() + 1;
		trip.setID(idTrip);
		// setTimenow
		Date now = new Date();
		Timestamp timestamp = new Timestamp(now.getTime());
		trip.setStationInTime(timestamp);
		tripDAO.insertTrip(trip, ticketOneway.getID(), idStation);
	}

	/**
	 * Check-out one way ticket' status
	 * 
	 * @return String This return null if status is valid, otherwise return string
	 *         error if status is invalid
	 */
	@Override
	public String checkStatusOut() {
		switch (ticketOneway.getStatus()) {
		case "new":
			return "The ticket's status is \"New\". You can check in before check out station.";
		case "in":
			return null;
		default:
			return "The ticket's status is \"Destroy\".";
		}
	}

	/**
	 * Check-out price of one way ticket
	 * 
	 * @param stationInRealID This is id of station in real
	 * @param stationOutReal  This is id of station out real
	 * @param priceInTicket   This is price of ticket
	 * @return String This is null if price valid otherwise return string error if
	 *         price invalid
	 */
	private String checkPrice(int stationInRealID, int stationOutRealID, double priceInTicket) {
		StationDAO stationDAO = new StationDAO();
		double locationIn = stationDAO.getLocationByID(stationInRealID);
		double locationOut = stationDAO.getLocationByID(stationOutRealID);
		double distance = Math.abs(locationIn - locationOut);
		
		CaculatorPrice caculatorPrice = new CaculatorPriceByDistanceOneLine(distance);
		double priceReal = caculatorPrice.caculatorPrice();
		if (priceReal <= priceInTicket) {
			return null;
		}
		return "Not enough balance: Expected " + priceReal + " euros.";
	}

	/**
	 * This function is checkout one way ticket, return error message or "OK"
	 */
	@Override
	public String checkOut() {
		String statusMessage = checkStatusOut();

		if (statusMessage != null) {
			return statusMessage;
		} else {
			TripDAO tripDAO = new TripDAO();
			int stationInRealID = tripDAO.getStationInRealID(ticketOneway.getID());

			String priceMessage = checkPrice(stationInRealID, idStation, ticketOneway.getPrice());
			if (priceMessage != null) {
				return priceMessage;
			} else {
				ticketOneway.setStatus("destroy");
				ticketOneway.changeStatusOut();
				// save trip
				tripDAO.saveStationOut(ticketOneway.getID(), idStation);
			}
		}
		return "OK";
	}
}
