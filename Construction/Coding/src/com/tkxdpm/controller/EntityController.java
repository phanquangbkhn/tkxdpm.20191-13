package com.tkxdpm.controller;

import com.tkxdpm.entity.Entity;

public abstract class EntityController {
	protected Entity entity;
	protected int idStation;

	public EntityController() {
	}

//	public EntityController(Entity entity, int idStation) {
//		this.entity = entity;
//		this.idStation = idStation;
//	}

	public abstract String checkStatusIn();

	public abstract String checkStatusOut();

	public abstract String checkIn();

	public abstract String checkOut();
}
