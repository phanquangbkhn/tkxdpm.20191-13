package com.tkxdpm.controller.caculatorPrice;

public class CaculatorPriceByZone implements CaculatorPrice{
	private int zoneIn;
	private int zoneOut;
	public CaculatorPriceByZone(int zoneIn, int zoneOut) {
		this.zoneIn = zoneIn;
		this.zoneOut = zoneOut;
	}
	
	@Override
	public double caculatorPrice() {
		switch (Math.abs(zoneIn - zoneOut)) {
		case 0:
			return 1.5;
		case 1:
			return 1.9;	
		default:
			return 2.5;
		}
	}
}
