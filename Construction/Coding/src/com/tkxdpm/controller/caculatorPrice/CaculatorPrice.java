package com.tkxdpm.controller.caculatorPrice;

public interface CaculatorPrice {
	public static final double BASE_PRICE = 1.9;
	public static final double BASE_DISTANCE = 5.0;
	/**
	 * This function is calculate price one ticket
	 * @return price
	 */
	public double caculatorPrice();
}
