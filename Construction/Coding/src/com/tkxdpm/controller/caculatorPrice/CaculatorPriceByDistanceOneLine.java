package com.tkxdpm.controller.caculatorPrice;

public class CaculatorPriceByDistanceOneLine implements CaculatorPrice{
	private double distance;
	
	public CaculatorPriceByDistanceOneLine(double distance) {
		this.distance = distance;
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public double caculatorPrice() {
		double price = BASE_PRICE;
		if (distance > 5.0) {
			if (distance - (int) distance != 0) {
				price += ((int) ((distance - BASE_DISTANCE) / 2) + 1) * 0.4;
			} else {
				price += (int) ((distance - BASE_DISTANCE) / 2) * 0.4;
			}
		}
		return price; 
	}
}
