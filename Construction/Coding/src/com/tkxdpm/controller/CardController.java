package com.tkxdpm.controller;

import java.sql.Timestamp;
import java.util.Date;
import com.tkxdpm.dao.CardDAO;
import com.tkxdpm.dao.StationDAO;
import com.tkxdpm.dao.TripDAO;
import com.tkxdpm.entity.Card;
import com.tkxdpm.entity.Trip;

public class CardController extends EntityController {
	private Card card;
	public CardController() {
		
	}
	public CardController(Card card, int idStation) {
		this.card = card;
		this.idStation = idStation;
	}
	
	
    public String checkStatusIn() {
        switch(card.getStatus()) {
            case "new":
                return null;
            case "out":
                return null;
            case "in":
                return "Card's status is \"In\"" + "!!! Please choose other card!";
            default:
                return "Card's status is \"Destroy\"" + "!!! Please choose other card!";
        }
    }


    public boolean checkStatusNew() {
        if(card.getStatus().equals("new")) {
            return true;
        }
        return false;
    }


    public void changeStatusIn() {
        card.setStatus("in");
        CardDAO cardDAO = new CardDAO();
        cardDAO.updateStatusInCard(card);
    }


    public boolean checkBallanceIsEnough() {
        if (card.getBalance() < 5) {
            return false;
        }
        return true;
    }



    public String checkIn() {
        String statusMessage = checkStatusIn();
        if(statusMessage != null) {
            return statusMessage;
        }

        if (!checkBallanceIsEnough()) {
            return "Your balance is not enough for this action !!!";
        }
        changeStatusIn();
        generateTrip(card, idStation);
        return "OK";
    }

    public boolean checkStationInReal(Card card, int idStationInReal) {
        StationDAO stDAO = new StationDAO();
        String stationIn = card.getStationIn();
        double locationIn = stDAO.getLocationByName(stationIn);
        String stationOut = card.getStationOut();
        double locationOut = stDAO.getLocationByName(stationOut);
        double locationInReal = stDAO.getLocationByID(idStationInReal);

        if(locationIn < locationInReal && locationInReal < locationOut) {
            return true;
        }else if(locationIn > locationInReal && locationInReal > locationOut){
            return true;
        }
        return false;
    }

    // generate ra trip
    public void generateTrip(Card card, int idStation) {
        TripDAO  tripDAO = new TripDAO();
        Trip trip = new Trip();
        // setID
        int idTrip = tripDAO.getMaxID() + 1;
        trip.setID(idTrip);
        // setTimenow
        Date now = new Date();
        Timestamp timestamp = new Timestamp(now.getTime());
        trip.setStationInTime(timestamp);
        tripDAO.insertTrip(trip, card.getID(), idStation);
    }

    /**
     * Calculate the price of card
     *
     * @param locationIn  This is location of station in real
     * @param locationOut This is location of station out real
     * @return double This return cost of this trip of card
     */
    private double caculatorPrice(double locationIn, double locationOut) {
        double distance = Math.abs(locationOut - locationIn);
        double price = 1.9;
        if (distance > 5.0) {
            if (distance - (int) distance != 0) {
                price += ((int) ((distance - 5.0) / 2) + 1) * 0.4;
            } else {
                price += (int) ((distance - 5.0) / 2) * 0.4;
            }
        }
        return price;
    }

    /**
     * Check-out card' status
     *
     * @param status This is "in" if status valid or "new" and "destroy" if status
     *               is invalid
     * @return String This return null if status is valid, otherwise return string
     *         error if status is invalid
     */
    private String checkStatusOut(String status) {
        switch (status) {
            case "new":
                return "The card's status is \"New\". You can check in before check out station.";
            case "in":
                return null;
            default:
                return "The card's status is \"Destroy\".";
        }
    }

    /**
     * Check-out price of card
     *
     * @param stationInRealID This is id of station in real
     * @param stationOutReal  This is id of station out real
     * @return price
     */
    private double checkPrice(int stationInRealID, int stationOutReal) {
        StationDAO stationDAO = new StationDAO();
        double locationIn = stationDAO.getLocationByID(stationInRealID);
        double locationOut = stationDAO.getLocationByID(stationOutReal);
        double priceReal = caculatorPrice(locationIn, locationOut);
        System.out.println("Price: " + priceReal + "$");
        return priceReal;
    }

    /**
     * Check-out card
     * @return String This return "OK" if result is true otherwise return string
     *         error if result is false
     */
    public String checkOut() {
        String statusMessage = checkStatusOut(card.getStatus());
        if (statusMessage != null) {
            return statusMessage;
        } else {
            TripDAO tripDAO = new TripDAO();
            int stationInRealID = tripDAO.getStationInRealID(card.getID());
            double price = checkPrice(stationInRealID, idStation);
            if (price > card.getBalance()) {
                return "Not enough balance: Expected " + price + " euros.";
            } else {
                CardDAO cardDAO = new CardDAO();
                card.setBalance(card.getBalance() - price);
                cardDAO.updateBalance(card);
                card.setStatus("out");
                cardDAO.updateStatusInCard(card);
                // save trip
                tripDAO.saveStationOut(card.getID(), idStation);
            }
        }
        return "OK";
    }
	@Override
	public String checkStatusOut() {
		// TODO Auto-generated method stub
		return null;
	}

}
