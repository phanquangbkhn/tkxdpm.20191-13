package com.tkxdpm.controller;



import java.util.ArrayList;
import java.util.HashMap;
import com.tkxdpm.entity.Card;
import com.tkxdpm.entity.Entity;
import com.tkxdpm.entity.Station;
import com.tkxdpm.entity.Ticket24h;
import com.tkxdpm.entity.TicketOneway;
import com.tkxdpm.utils.FilterFile;
import com.tkxdpm.utils.ReadFile;
import com.tkxdpm.utils.RenderCode;

import hust.soict.se.gate.Gate;
import com.tkxdpm.dao.CardDAO;
import com.tkxdpm.dao.StationDAO;
import com.tkxdpm.dao.Ticket24hDAO;
import com.tkxdpm.dao.TicketOnewayDAO;

public class AFCController {
	private EntityController entityController;
	
	public AFCController() {
		
	}
	public void setEntityController(EntityController entityController) {
		this.entityController = entityController;
	}
	public EntityController getEntityController() {
		return entityController;
	}
	
	public ArrayList<Station> getAllStation() {
		ArrayList<Station> listStation = new ArrayList<Station>();
		StationDAO std = new StationDAO();
		listStation = std.getAllStation();
		return listStation;
	}

	/**
	 * 
	 * @return HashMap This return list ticket or card object
	 */
	public HashMap<String, Entity> getMapEntityByBarcode(){
		String path = "./listBarcode/listBarcode.txt";
		ArrayList<String> listBarcode = ReadFile.readFile(path);
		FilterFile ff = new FilterFile();
		ArrayList<String> listFilterBarcode = ff.filterFile(listBarcode); 
		HashMap<String, Entity> mapBarcodeEntity = new HashMap<String, Entity>();
		for (String barcode : listFilterBarcode) {
			Entity entity = getEntityByBarcode(barcode);
			mapBarcodeEntity.put(barcode, entity);
		}
		return mapBarcodeEntity;
	}	
	/**
	 * 
	 * @param barcode This is barcode string of entity you want to get info
	 * @return entity, it is one way ticket, 24h ticket or prepaid card
	 */
	public Entity getEntityByBarcode(String barcode) {
		RenderCode rc = new RenderCode();
		String code = null;
		if(Character.isUpperCase(barcode.charAt(0))) {
			code = rc.renderCardCode(barcode);
			CardDAO cardDAO = new CardDAO();
		    Card card = cardDAO.getCardByCode(code);
		    return card;
		}
		code = rc.renderTicketCode(barcode);	
		TicketOnewayDAO toDAO = new TicketOnewayDAO();
		TicketOneway ticketOneway = toDAO.getTicketOnewayByCode(code);
		if(ticketOneway != null) {
			return ticketOneway;
		}
		Ticket24hDAO t24hDAO = new Ticket24hDAO();
		Ticket24h ticket24h = t24hDAO.getTicket24hByCode(code);
		if(ticket24h != null) {
			return ticket24h;
		}
		return null;
		
	}

	/**
	 * This method will open the gate and wait for 1s to close the gate
	 */
	public void openAndCloseGate() {
		Gate gate = Gate.getInstance();
		gate.open();
		try {
			Thread.sleep(6000);
			gate.close();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method will close the gate
	 */
	public void closeGate() {
		Gate gate = Gate.getInstance();
		gate.close();
	}
}
