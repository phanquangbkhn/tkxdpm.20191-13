package com.tkxdpm.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import com.tkxdpm.entity.Card;

public class CardDAO {

    /**
     * This function is used to get one instance of Card class from is's code
     * @param code Code is convert from barcode
     * @return Card One instance of Card class
     */
    public Card getCardByCode(String code) {
        Card card = new Card();
        try {
            Connection conn = DBConnector.getInstance().getConnection();
            String sqlString = "select * from card where code=\""+ code + "\"";
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(sqlString);
            if(!rs.next()) {
                return null;
            } else {
                do {
                    card.setID(rs.getString(1));
                    card.setCode(rs.getString(2));
                    card.setStatus(rs.getString(3));
                    card.setBalance(rs.getDouble(4));
                    card.setCreateAt(rs.getTimestamp(5));
                }
                while(rs.next());
            }

        } catch(SQLException e) {
            System.out.println("Error StationDAO");
            return null;
        }
        return card;
    }
    /**
     * This function is used to update status of card
     * @param card Instance of Card class
     */
    public void updateStatusInCard(Card card) {
        try{
            Connection conn = DBConnector.getInstance().getConnection();
            String sqlString = "update card set status=\""+ card.getStatus() + "\"";
            sqlString += " where id = \"" + card.getID() + "\"";
            Statement stm = conn.createStatement();
            stm.executeUpdate(sqlString);
        } catch(SQLException e) {
            System.out.println("Error CardDAO update");
        }
    }

    public void updateBalance(Card card) {
        try{
            Connection conn = DBConnector.getInstance().getConnection();
            String sqlString = "update card set balance=\""+ card.getBalance() + "\"";
            sqlString += " where id = \"" + card.getID() + "\"";
            Statement stm = conn.createStatement();
            stm.executeUpdate(sqlString);
        } catch(SQLException e) {
            System.out.println("Error CardDAO update");
        }
    }

}
