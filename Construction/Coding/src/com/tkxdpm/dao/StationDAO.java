package com.tkxdpm.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.tkxdpm.entity.Station;

public class StationDAO {
	
	/**
	 * This function is used to return one array list contains station Objects
	 * @return ArrrayList{@literal <}Station{@literal >} One array list contains instances of Station class
	 */

	public ArrayList<Station> getAllStation() {
		ArrayList<Station> listStation = new ArrayList<Station>();
		try {
			Connection conn = DBConnector.getInstance().getConnection();
			String sqlString = "select * from station";
			Statement stm = conn.createStatement();

			ResultSet rs = stm.executeQuery(sqlString);
			while(rs.next()) {
				Station station = new Station(rs.getInt(1), rs.getString(2), rs.getDouble(3));
				listStation.add(station);
			}
			
		} catch(SQLException e) {
			System.out.println("Error StationDAO");
			return null;
		}
		return listStation;
	}
	/**
	 * This function is used to get name of station from id input
	 * @param id index of station 
	 * @return String The name of station
	 */
	public String getNameByID(int id) {
		String name = null;
		try {
			Connection conn = DBConnector.getInstance().getConnection();
			String sqlString = "select name from station where id = " + id;
			Statement stm = conn.createStatement();
			ResultSet rs = stm.executeQuery(sqlString);
			while(rs.next()) {
				name = rs.getString(1);
			}		
		} catch(SQLException e) {
			System.out.println("Error StationDAO aaa");
			return null;
		}
		return name;
	}
	
	/**
	 * This function is used to get location of station
	 * @param id Index of station
	 * @return double Location of station
	 */
	public double getLocationByID(int id) {
		double location = 0;
		try {
			Connection conn = DBConnector.getInstance().getConnection();
			String sqlString = "select location from station where id = \"" + id+"\"";
			Statement stm = conn.createStatement();
			ResultSet rs = stm.executeQuery(sqlString);
			while(rs.next()) {
				location = rs.getDouble(1);
			}		
		} catch(SQLException e) {
			System.out.println("Error! Cannot get location");
			return 0;
		}
		return location;
	}
	/**
	 * 
	 * @param stationID station id you want to get zone
	 * @return zone of station
	 */
	public int getZoneByID(int stationID) {
		int zone = -1;
		try {
			Connection conn = DBConnector.getInstance().getConnection();
			String sqlString = "select zone from station where id = " + stationID;
			Statement stm = conn.createStatement();
			ResultSet rs = stm.executeQuery(sqlString);
			while(rs.next()) {
				zone = rs.getInt(1);
			}		
		} catch(SQLException e) {
			System.out.println("Error! Zone of station is null");
			return -1;
		}
		return zone;
	}
	
	/**
	 * This function is used to get location fron station's name input
	 * @param name The name of stations
	 * @return	double Location of station
	 */
	public double getLocationByName(String name) {
		double location = 0;
		try {
			Connection conn = DBConnector.getInstance().getConnection();
			String sqlString = "select location from station where name = \"" + name +"\"";
			Statement stm = conn.createStatement();
			ResultSet rs = stm.executeQuery(sqlString);
			while(rs.next()) {
				location = rs.getDouble(1);
			}		
		} catch(SQLException e) {
			System.out.println("Error StationDAO");
			return 0;
		}
		return location;
	}
	
	
}
