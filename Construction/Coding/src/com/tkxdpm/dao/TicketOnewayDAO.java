package com.tkxdpm.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.tkxdpm.entity.TicketOneway;

public class TicketOnewayDAO {

	/**
	 * This function is used to get one ticket one-way Object from it's code input
	 * 
	 * @param code Code of ticket one-way
	 * @return TicketOneway One instance of TicketOneway
	 */
	public TicketOneway getTicketOnewayByCode(String code) {
		TicketOneway ticketOneway = new TicketOneway();
		try {
			Connection conn = DBConnector.getInstance().getConnection();
			String sqlString = "select * from ticket_one_way where code=\"" + code + "\"";
			Statement stm = conn.createStatement();
			ResultSet rs = stm.executeQuery(sqlString);
			if (!rs.next()) {
				return null;
			} else {
				do {
					StationDAO std = new StationDAO();
					ticketOneway.setID(rs.getString(1));
					ticketOneway.setCode(rs.getString(2));
					ticketOneway.setStatus(rs.getString(3));
					ticketOneway.setPrice(rs.getDouble(4));
					String nameStationIn = std.getNameByID(rs.getInt(5));
					ticketOneway.setStationIn(nameStationIn);
					String nameStationOut = std.getNameByID(rs.getInt(6));
					ticketOneway.setStationOut(nameStationOut);
					ticketOneway.setCreateAt(rs.getTimestamp(7));
				} while (rs.next());
			}
		} catch (SQLException e) {
			System.out.println("Error! Cannot get one way ticket by code!");
			return null;
		}
		return ticketOneway;
	}
	

	/**
	 * This method change one way ticket's status
	 * @param ticketID This is ticket id if ticket object you wanto to change status
	 * @param status This is status you want to change
	 */
	public void changeStatus(String ticketID, String status) {
		try {
			Connection conn = DBConnector.getInstance().getConnection();
			String sqlString = "update ticket_one_way set status=\"" + status + "\"";
			sqlString += " where id = \"" + ticketID + "\"";
			Statement stm = conn.createStatement();
			stm.executeUpdate(sqlString);
		} catch (SQLException e) {
			System.out.println("Error! Cannot update status this one way ticket!");
		}
	}
}
